class @EcoPortal.UI
  init: ->
  applyRandomClass: ->
    randomClasses = ["blue", "yellow", "green"]
    $(".data-content").each ->
      $(this).addClass(randomClasses[Math.floor(Math.random()*randomClasses.length)])
      #running out of time thats why i used this
      $(this).html("<p contenteditable='true'>#{$(this).html()}</p>")
      $(this).append("<span class='ui-icon'></span>")
  applySortable: ->
    $(".column-content-container").sortable({connectWith: ".column-content-container", handle: ".ui-icon"})
    $(".column-content-container").enableSelection()
