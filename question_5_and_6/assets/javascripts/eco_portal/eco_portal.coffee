class @Singleton
  @getInstance: ->
    @_instance ?=  new @(arguments...)

class @EcoPortal extends Singleton
  constructor: ->
    @UI = new EcoPortal.UI()
