require 'bundler'
Bundler.require
require 'sinatra/asset_pipeline'

class App < Sinatra::Base
  register Sinatra::AssetPipeline
  helpers { include JqueryCdn::Helpers }
  get "/" do
    haml :index
  end
end
